package main

import (
	"os"
	"encoding/json"
	"strings"
	"time"
	"log"
)

type output struct {
	Items []wikiItem
}

func isRaidItem(item wikiItem) bool {
	switch(strings.ToLower(item.Quest)) {
		case "plane of night": fallthrough
		case "the titan awakes": fallthrough
		case "zawabi's revenge": fallthrough
		case "a vision of destruction": fallthrough
		case "hound of xoriat": fallthrough
		case "ascension chamber": fallthrough
		case "tower of despair": fallthrough
		case "the chronoscope": fallthrough
		case "the twilight forge": fallthrough
		case "the shroud": fallthrough
		case "the codex and the shroud": fallthrough
		case "legendary hound of xoriat": fallthrough
		case "legendary tempest's spine": fallthrough
		case "the fall of truth": fallthrough
		case "cought in the web": fallthrough
		case "fire on thunder peak": fallthrough
		case "defiler of the just": fallthrough
		case "riding the storm out": fallthrough
		case "the curse of strahd": fallthrough
		case "old baba's hut": fallthrough
		case "killing time": fallthrough
		case "too hot to handle": fallthrough
		case "project nemesis": fallthrough
		case "legendary vision of destruction": fallthrough
		case "legendary master artificer": fallthrough
		case "legendary lord of blades": fallthrough
		case "temple of the deathwyrm": fallthrough
		case "the mark of death": fallthrough
		case "the dryad and the demigod": fallthrough
		case "the lord of blades": fallthrough
		case "the master artificer": fallthrough
		case "the vault of night": fallthrough
		case "the reaver's fate": fallthrough
	case "tempest's spine":
		return true
	}

	return false
}

func itemSlot(item wikiItem) string {
	switch(strings.ToLower(item.Type)) {
		case "short bow": fallthrough
		case "dart": fallthrough
		case "falchion": fallthrough
		case "great axe": fallthrough
		case "great club": fallthrough
		case "great crossbow": fallthrough
		case "great sword": fallthrough
		case "greatclub": fallthrough
		case "handwrap": fallthrough
		case "light crossbow": fallthrough
		case "long bow": fallthrough
		case "maul": fallthrough
		case "quarterstaff": fallthrough
		case "repeating heavy crossbow": fallthrough
		case "repeating light crossbow": fallthrough
		case "shuriken": fallthrough
		case "throwing axe": fallthrough
		case "throwing dagger": fallthrough
		case "throwing hammer": fallthrough
	case "heavy crossbow":
		return "Main Hand"

		case "buckler": fallthrough
		case "large": fallthrough
		case "orb": fallthrough
	case "small":
		return "Off Hand"

		case "bastard sword": fallthrough
		case "battle axe": fallthrough
		case "dagger": fallthrough
		case "dwarven war axe": fallthrough
		case "hand axe": fallthrough
		case "handaxe": fallthrough
		case "heavy mace": fallthrough
		case "heavy pick": fallthrough
		case "kama": fallthrough
		case "khopesh": fallthrough
		case "kukri": fallthrough
		case "light hammer": fallthrough
		case "light mace": fallthrough
		case "light pick": fallthrough
		case "long sword": fallthrough
		case "longsword": fallthrough
		case "morningstart": fallthrough
		case "rapier": fallthrough
		case "scimitar": fallthrough
		case "short sword": fallthrough
		case "sickle": fallthrough
		case "war hammer": fallthrough
		case "warhammer": fallthrough
	case "club":
		return "Main Hand, Off Hand"

		case "docent": fallthrough
		case "heavy armor": fallthrough
		case "light armor": fallthrough
		case "medium armor": fallthrough
	case "robe":
		return "Armor"

		case "collar": fallthrough
	case "module":
		 return "Pet Collar"

		case "cosmetic sceptre": fallthrough
	case "cosmetic long sword":
		return "Cosmetic Main Hand, Cosmetic Off Hand"

	case "cosmetic quarterstaff":
		return "Cosmetic Main Hand"
	}

	return ""
}

func main() {
	var logger = log.New(log.Writer(), "Main:", log.Ltime)

	categories := os.Args[1:]
	if len(categories) == 0 {
		logger.Fatal("Please pass categories to download")
	}

	var parsedData output

	for _, category := range categories {
		logger.Println("Downloading:", category)

		finished := false
		for page:=""; !finished; {
			data := downloadData(category, page)

			for _, item := range data.Query.Pages {
				if strings.Contains(item.Title, "Item") {
					logger.Println(item.Title)
					var witem wikiItem
					witem.Page = item.Title
					unmarshalTemplate(item.Revisions[0].Content, &witem)

					if witem.Name == "" {
						logger.Println("Template missing name")
						continue
					}

					witem.Raiditem = isRaidItem(witem)

					if witem.Slot == "" {
						witem.Slot = itemSlot(witem)
					}

					parsedData.Items = append(parsedData.Items, witem)
				}
			}

			page = data.Continue.Gcmcontinue
			if page == "" {
				finished = true
			}

			time.Sleep(3 * time.Second)
		}
	}

	logger.Println("Writing Json File...")
	json, _ := json.Marshal(parsedData)
	err := os.WriteFile("output.json", json, 0644)
	if err != nil {
		panic(err)
	}
}
