package main

import (
	"regexp"
	"reflect"
	"strings"
	"strconv"
	"net/http"
	"net/url"
	"time"
	"log"

	"github.com/PuerkitoBio/goquery"
)

// Monolithic item type. Omitempty so json output only contains the relevent
// fields for each item. Fields taged '-' will never to emitted, used to select
// the fields we actually display on the site
type wikiItem struct {
	// All Items
	Bind string `json:",omitempty"`
	Enhancements []string `json:",omitempty"`
	Minlevel int `json:",omitempty"`
	Name string `json:",omitempty"`
	Slot string `json:",omitempty"`
	// Weapons
	Attackmod string `json:",omitempty"`
	Crit string `json:",omitempty"`
	Damage string `json:",omitempty"`
	Damagemod string `json:",omitempty"`
	Prof string `json:",omitempty"`
	Type string `json:",omitempty"`
	UMDDC string `json:",omitempty"`
	// Armor
	Ac string `json:",omitempty"`
	Asf string `json:",omitempty"` // Arcane spell failure
	Maxdexbonus string `json:",omitempty"`
	// Docent
	Race string `json:",omitempty"`
	// Internal
	Page string `json:",omitempty"`
	Raiditem bool `json:",omitempty"`

	//Parsed but not output
	Armortype string `json:"-,omitempty"`
	Basevalue string `json:"-,omitempty"`
	Chest string `json:"-,omitempty"`
	Class string `json:"-,omitempty"`
	Crafting string `json:"-,omitempty"`
	Craftingupgrade string `json:"-,omitempty"`
	Description string `json:"-,omitempty"`
	Durability string `json:"-,omitempty"`
	Feat string `json:"-,omitempty"`
	Hardness string `json:"-,omitempty"`
	Material string `json:"-,omitempty"`
	Mythic string `json:"-,omitempty"`
	Notes string `json:"-,omitempty"`
	Pic string `json:"-,omitempty"`
	Picdescription string `json:"-,omitempty"`
	Quest string `json:"-,omitempty"`
	Released string `json:"-,omitempty"`
	Reqtrait string `json:"-,omitempty"`
	Weight string `json:"-,omitempty"`
}

var templateCache = make(map[string]string)

//var templateRegex = regexp.MustCompile(`.*\| *([a-z]*) *= *([\w]+.*|{{.*)`)
var templateRegex = regexp.MustCompile(`.*\| *[a-zA-Z]* *=.*`)
var expansionRegex = regexp.MustCompile(`{{.*}}`)
var enhancementRegex = regexp.MustCompile(`\| *enhancements *=.*\n(?:\*.*\n)*`)
var listRegex = regexp.MustCompile(`\*.*`)

func templateEnhancements(template string) []string {
	var enhList = make([]string,0)

	for _, enhancements := range enhancementRegex.FindAllSubmatch([]byte(template), -1) {
		for _, listItem := range listRegex.FindAllSubmatch(enhancements[0], -1) {
			item := string(listItem[0])
			item = strings.TrimLeft(item, "* ")
			item = strings.TrimRight(item, " ")

			if expansionRegex.Match([]byte(item)) {
				item = expandTemplate(item)
			}

			enhList = append(enhList, item)
		}
		break
	}

	return enhList
}

// Dirty template unmarshal function. Uses reflection to match key value from
// template onto a struct for json marshal later on.
func unmarshalTemplate(template string, obj interface{}) {
	objValue := reflect.ValueOf(obj).Elem()

	if objValue.Kind() == reflect.Struct {
		// Loop ever every capture group based on template
		// Example: |group1 = group2
		for _, matches := range templateRegex.FindAllSubmatch([]byte(template), -1) {
			for _, match := range matches {
				split := strings.SplitN(string(match), "=", 2)

				if len(split) >= 2 {
					templateField := split[0]
					templateField = strings.Trim(templateField, "| ")

					templateValue := split[1]
					templateValue = strings.TrimLeft(templateValue, "| ")
					templateValue = strings.TrimRight(templateValue, "| ")

					field := objValue.FieldByName(strings.Title(templateField))
					// If the field exists on the obj we are passed, then set the value
					if field.IsValid() {
						if templateField == "enhancements" {
							enhacements := templateEnhancements(template)
							field.Set(reflect.ValueOf(enhacements))
						} else {
							if expansionRegex.Match([]byte(templateValue)) {
								templateValue = expandTemplate(templateValue)
							}
							switch(field.Type().Name()) {
								case "string":
									// check if the line is just a comment
									if !strings.Contains(templateValue, "<!--") {
										field.SetString(templateValue)
									}
								case "int":
									asInt, err := strconv.Atoi(templateValue)
									if err != nil {
										field.Set(reflect.ValueOf(0))
									} else {
										field.Set(reflect.ValueOf(asInt))
									}
							}
						}
					}
				}
			}
		}
	}
}

var parseURL = "https://ddowiki.com/api.php?action=parse&contentmodel=wikitext&prop=text&format=json&text="
func expandTemplate(template string) string {
	var logger = log.New(log.Writer(), "expandTemplate:", log.Ltime)

	upperTemplate := strings.ToUpper(template)

	result, exists := templateCache[upperTemplate]
	if exists {
		logger.Println("USING CACHED TEMPLATE:", template)
		return result
	}
	logger.Println("EXPANDING: ", template)

	res, err := http.Get(parseURL + url.QueryEscape(template))
	if err != nil {
		logger.Fatal(err)
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		logger.Fatal(err)
	}

	var templateOutput = ""
	if doc.Find("table").Length() > 0 {
		templateOutput, err = goquery.OuterHtml(doc.Find("table").First())
		if err != nil {
			logger.Fatal(err)
		}
	} else {
		templateOutput = doc.Find("a").First().Text()
	}

	templateOutput = strings.TrimLeft(templateOutput, " ")
	templateOutput = strings.TrimRight(templateOutput, " ")

	logger.Println(templateOutput)
	templateCache[upperTemplate] = templateOutput
	time.Sleep(1 * time.Second)
	return templateOutput
}
