package main

import (
	"net/http"
	"net/url"
	"io"
	"encoding/json"
	"log"
)

var categorymembersApiURL = "https://ddowiki.com/api.php?action=query&generator=categorymembers&gcmlimit=50&prop=revisions&rvprop=content&format=json&gcmtitle="

type categorymembers struct {
	Query Query
	Continue Continue
}

type Continue struct {
	Gcmcontinue string
	Continue string
}

type Query struct {
	Pages map[string]Page
}

type Page struct {
	Pageid uint
	Title string
	Revisions []Revision
}

type Revision struct {
	Content string `json:"*"`
}

func downloadData(category string, page string) categorymembers {
	var logger = log.New(log.Writer(), "downloadData:", log.Ltime)

	URL := categorymembersApiURL
	URL += url.QueryEscape(category)
	if page != "" {
		URL += "&gcmcontinue=" + url.QueryEscape(page)
	}

	resp, err := http.Get(URL)
	if err != nil {
		logger.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		logger.Fatal(err)
	}

	var catmembers categorymembers
	json.Unmarshal(body, &catmembers)
	
	return catmembers
}
